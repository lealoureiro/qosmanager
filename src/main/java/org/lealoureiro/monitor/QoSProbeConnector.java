package org.lealoureiro.monitor;

import org.apache.log4j.Logger;
import org.lealoureiro.monitor.db.DatabaseAccess;
import org.snmp4j.*;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.AuthSHA;
import org.snmp4j.security.PrivAES128;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.*;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TableEvent;
import org.snmp4j.util.TableUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Probe Connector for transfer information
 *
 * @author Leandro Loureiro <lelaoureiro@gmail.com>
 * @version 1.0.0
 */
public class QoSProbeConnector extends Thread {

    private static Logger log = Logger.getLogger(QoSProbeConnector.class);

    private final OID PASS_HASH_ALGO = AuthSHA.ID;
    private final OID ENC_ALGO = PrivAES128.ID;
    private final OID NEW_JOB_ID_OID = new OID(".1.3.6.1.3.2.1.1");
    private final OID NEW_IP_OID = new OID(".1.3.6.1.3.2.2.1");
    private final OID NEW_REQUESTS_OID = new OID(".1.3.6.1.3.2.3.1");
    private final OID NEW_POLLING_TIME_OID = new OID(".1.3.6.1.3.2.4.1");
    private final OID RESULTS_TABLE_OID = new OID(".1.3.6.1.3.1");
    private final OID DELETE_RESULT_ID_OID = new OID(".1.3.6.1.3.2.1.2");
    private String probeId;
    private String ip;
    private int port;
    private OctetString username;
    private OctetString password;
    private OctetString encryptionPassphrase;
    private UsmUser currentUser;
    private TransportMapping transport;
    private Snmp snmpHost;
    private Address targetAddress;
    private UserTarget target;
    private DatabaseAccess dbAccess;
    private ProbesTable probesTable;
    private OID[] tableColumnsOIDs;
    private volatile int pollingTime;
    private volatile boolean alive;

    public QoSProbeConnector(String probeId, String ip, int port, DatabaseAccess dbAccess) {
        this.probeId = probeId;
        this.ip = ip;
        this.port = port;
        this.pollingTime = 10000;
        this.dbAccess = dbAccess;
        this.alive = true;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String getProbeId() {
        return probeId;
    }

    public int getPollingTime() {
        return pollingTime;
    }

    public void setPollingTime(int newPollingTime) {
        this.pollingTime = newPollingTime;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public String getAuthUsername() {
        return this.username.toString();
    }

    public String getAuthPassword() {
        return this.password.toString();
    }

    public String getAuthPassphrase() {
        return this.encryptionPassphrase.toString();
    }

    public void setProbesTable(ProbesTable probesTable) {
        this.probesTable = probesTable;
    }

    public void setCredentials(String username, String password, String passPhrase) {
        this.username = new OctetString(username);
        this.password = new OctetString(password);
        this.encryptionPassphrase = new OctetString(passPhrase);

        this.currentUser = new UsmUser(this.username, this.PASS_HASH_ALGO, this.password, this.ENC_ALGO, this.encryptionPassphrase);
    }

    public void setNewCredentials(String username, String password, String passPhrase) {

        this.snmpHost.getUSM().removeUser(new OctetString(MPv3.createLocalEngineID()), this.username);

        this.username = new OctetString(username);
        this.password = new OctetString(password);
        this.encryptionPassphrase = new OctetString(passPhrase);

        this.currentUser = new UsmUser(this.username, this.PASS_HASH_ALGO, this.password, this.ENC_ALGO, this.encryptionPassphrase);
        this.snmpHost.getUSM().addUser(this.username, currentUser);
        this.target.setSecurityName(new OctetString(this.username));

    }

    public void init()
            throws IOException {

        this.transport = new DefaultUdpTransportMapping();
        transport.listen();
        this.snmpHost = new Snmp(transport);
        this.snmpHost.getUSM().addUser(this.username, currentUser);
        this.targetAddress = GenericAddress.parse("udp:" + this.ip + "/" + this.port);
        target = new UserTarget();
        target.setAddress(targetAddress);
        target.setRetries(2);
        target.setTimeout(1000);
        target.setVersion(SnmpConstants.version3);
        target.setSecurityLevel(SecurityLevel.AUTH_PRIV);
        target.setSecurityName(new OctetString(this.username));


        // prepare table columns oids
        this.tableColumnsOIDs = new OID[10];
        this.tableColumnsOIDs[0] = new OID(".1.3.6.1.3.1.1"); // jobId
        this.tableColumnsOIDs[1] = new OID(".1.3.6.1.3.1.2"); // ip
        this.tableColumnsOIDs[2] = new OID(".1.3.6.1.3.1.3"); // timestamp
        this.tableColumnsOIDs[3] = new OID(".1.3.6.1.3.1.4"); // rttNow
        this.tableColumnsOIDs[4] = new OID(".1.3.6.1.3.1.5"); // rttMin
        this.tableColumnsOIDs[5] = new OID(".1.3.6.1.3.1.6"); // rttMax
        this.tableColumnsOIDs[6] = new OID(".1.3.6.1.3.1.7"); // rttAvg
        this.tableColumnsOIDs[7] = new OID(".1.3.6.1.3.1.8"); // jitter
        this.tableColumnsOIDs[8] = new OID(".1.3.6.1.3.1.9"); // packetLost
        this.tableColumnsOIDs[9] = new OID(".1.3.6.1.3.1.10"); // alive
    }

    public synchronized void addJob(String jobId, String ipAddress, int requests, int pollingTime) throws IOException {

        PDU pdu = new ScopedPDU();
        pdu.setType(PDU.SET);

        pdu.add(new VariableBinding(this.NEW_JOB_ID_OID, new OctetString(jobId)));
        pdu.add(new VariableBinding(this.NEW_IP_OID, new OctetString(ipAddress)));
        pdu.add(new VariableBinding(this.NEW_REQUESTS_OID, new OctetString("" + requests)));
        pdu.add(new VariableBinding(this.NEW_POLLING_TIME_OID, new OctetString("" + pollingTime)));

        ResponseEvent send = this.snmpHost.send(pdu, this.target, this.transport);

        send.getResponse();

    }

    public void fetchResults() {

        DefaultPDUFactory factory = new DefaultPDUFactory();
        TableUtils tableUtils = new TableUtils(this.snmpHost, factory);
        List<TableEvent> events = tableUtils.getTable(target, this.tableColumnsOIDs, null, null);
        int i = 0;
        for (TableEvent event : events) {

            if (event.isError()) {
                log.warn("Table Event for probe " + this.probeId + " error: " + event.getErrorMessage());
                this.probesTable.getTableModel().setErrorMessage(this.probeId, event.getErrorMessage());
            } else {

                // store fetched result
                VariableBinding[] vbs = event.getColumns();

                String id = vbs[0].getVariable().toString();
                String timestamp = vbs[2].getVariable().toString();
                String rttNow = vbs[3].getVariable().toString();
                String rttMax = vbs[5].getVariable().toString();
                String rttMin = vbs[4].getVariable().toString();
                String rttAvg = vbs[6].getVariable().toString();
                String jitter = vbs[7].getVariable().toString();
                String packetLoss = vbs[8].getVariable().toString();
                String hostAlive = vbs[9].getVariable().toString();
                try {
                    this.dbAccess.storeReportData(id, timestamp, rttNow, rttMax, rttMin, rttAvg, packetLoss, jitter, hostAlive);
                } catch (SQLException ex) {
                    log.error("Failed to store report result " + id + ": " + ex.getMessage());
                }

                // delete fetched result on probe
                PDU pdu = new ScopedPDU();
                pdu.setType(PDU.SET);

                pdu.add(new VariableBinding(this.DELETE_RESULT_ID_OID, new OctetString(id)));
                try {
                    this.snmpHost.send(pdu, this.target, this.transport);
                } catch (IOException ex) {
                    this.probesTable.getTableModel().setErrorMessage(this.probeId, ex.getMessage());
                    log.error("Failed to delete report " + id + " on probe " + this.probeId + ": " + ex.getMessage());
                }

                i++;
            }
        }
        System.out.println("[INFO] fetched " + i + " results from " + this.probeId);
    }

    @Override
    public void run() {


        while (this.alive) {
            this.fetchResults();
            try {
                Thread.sleep(this.pollingTime);
            } catch (InterruptedException ex) {
                log.warn("Probe fetcher thread of probe " + this.probeId + " interrupted: " + ex.getMessage());
                this.probesTable.getTableModel().setErrorMessage(this.probeId, ex.getMessage());
            }
        }

        log.info("Probe connector thread finished");
    }
}
