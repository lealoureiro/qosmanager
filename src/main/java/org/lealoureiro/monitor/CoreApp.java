package org.lealoureiro.monitor;

import org.apache.log4j.Logger;
import org.lealoureiro.monitor.db.DatabaseAccess;
import org.snmp4j.mp.MPv3;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.smi.OctetString;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Core Application Class
 *
 * @author Leandro Loureiro <lealoureiro@gmail.com>
 * @version 1.0.0
 */

public class CoreApp {

    private static Logger log = Logger.getLogger(CoreApp.class);

    private USM usm;
    private DatabaseAccess dbAccess;
    private HashMap<String, QoSProbeConnector> probes;
    private HashMap<String, Float> probesRequestsRatio;
    private int jobsProcessed;
    private ProbesTable probesMIBTable;

    public CoreApp(DatabaseAccess dbAccess, ProbesTable probesMIBTable) {

        this.dbAccess = dbAccess;
        this.probes = new HashMap<String, QoSProbeConnector>();
        this.probesRequestsRatio = new HashMap<String, Float>();
        this.probesMIBTable = probesMIBTable;

        this.jobsProcessed = 0;

        log.info("Init USM Security System");
        this.usm = new USM(SecurityProtocols.getInstance(), new OctetString(MPv3.createLocalEngineID()), 0);
        SecurityModels.getInstance().addSecurityModel(this.usm);

    }

    public void initProbesConnectors() { // init probe from persistent storage

        ArrayList<QoSProbeConnector> probesTemp = null;

        try {
            probesTemp = this.dbAccess.getStoredProbes();
        } catch (Exception ex) {
            log.fatal("Failed to get probes from persistent storage: " + ex.getMessage());
            System.exit(1);
        }

        for (QoSProbeConnector probe : probesTemp) {

            probe.setProbesTable(this.probesMIBTable);
            this.probes.put(probe.getProbeId(), probe);
            this.probesRequestsRatio.put(probe.getProbeId(), 0.0f);
            this.probesMIBTable.getTableModel().addRow(probe.getProbeId(), probe.getIp(), probe.getPort(), probe.getPollingTime(), probe.getAuthUsername(), probe.getAuthPassword(), probe.getAuthPassphrase());

            try {
                probe.init();
                probe.start();
            } catch (IOException ex) {
                log.warn("Failed to init probe " + probe.getProbeId() + ": " + ex.getMessage());
                this.probesMIBTable.getTableModel().setErrorMessage(probe.getProbeId(), ex.getMessage());
            }
        }

    }

    public synchronized void addNewProbe(String probeId, String address, int port, String username, String password, String passPhrase)
            throws Exception {

        if (this.probes.containsKey(probeId)) {
            throw new Exception("Probe already added!");
        }

        QoSProbeConnector probe = new QoSProbeConnector(probeId, address, port, this.dbAccess);
        probe.setCredentials(username, password, passPhrase);
        probe.setProbesTable(this.probesMIBTable);

        this.probes.put(probeId, probe);
        this.probesRequestsRatio.put(probeId, 0.0f);

        try {
            probe.init();
        } catch (Exception ex) {
            this.probesMIBTable.getTableModel().setErrorMessage(probe.getProbeId(), ex.getMessage());
        }

        probe.start();

        this.dbAccess.storeProbeData(probeId, address, port, username, password, passPhrase, probe.getPollingTime());
        this.probesMIBTable.getTableModel().addRow(probe.getProbeId(), probe.getIp(), probe.getPort(), probe.getPollingTime(), probe.getAuthUsername(), probe.getAuthPassword(), probe.getAuthPassphrase());
    }

    public synchronized void deleteProbe(String probeId)
            throws Exception {

        if (!this.probes.containsKey(probeId)) {
            throw new Exception("Probe not found!");
        }

        QoSProbeConnector probe = this.probes.get(probeId);
        probe.setAlive(false);
        this.probes.remove(probeId);
        this.probesRequestsRatio.remove(probeId);
        this.dbAccess.removeProbe(probeId);

    }

    public synchronized void changeProbePollingTime(String probeId, int pollingTime)
            throws Exception {
        if (!this.probes.containsKey(probeId)) {
            throw new Exception("Probe not found!");
        }

        QoSProbeConnector probe = this.probes.get(probeId);
        probe.setPollingTime(pollingTime);
        this.dbAccess.setProbePollingTime(probeId, pollingTime);
    }

    public synchronized void changeProbeCredentials(String probeId, String username, String password, String passphrase)
            throws Exception {

        if (!this.probes.containsKey(probeId)) {
            throw new Exception("Probe not found!");
        }

        QoSProbeConnector probe = this.probes.get(probeId);
        probe.setNewCredentials(username, password, passphrase);
        this.dbAccess.setProbeCredentials(probeId, username, password, passphrase);

    }

    public synchronized void addJob(String ipAddress, int requests, int pollingTime) {

        String jobId = "";
        String selectedProbe = "";

        float tempRatio = Float.MAX_VALUE;

        for (Map.Entry<String, Float> ratio : this.probesRequestsRatio.entrySet()) {
            if (ratio.getValue() < tempRatio) {
                tempRatio = ratio.getValue();
                selectedProbe = ratio.getKey();
            }
        }

        // update probe work count
        float newRatio = this.probesRequestsRatio.get(selectedProbe);
        newRatio += (requests / pollingTime);
        this.probesRequestsRatio.put(selectedProbe, newRatio);
        log.info("Updated " + selectedProbe + " workload to " + newRatio);
        try {
            jobId = "" + this.dbAccess.createReportEntry(selectedProbe, ipAddress);
            this.probes.get(selectedProbe).addJob(jobId, ipAddress, requests, pollingTime);
        } catch (Exception ex) {
            log.error("Failed to submit job " + jobId + " to " + selectedProbe + ": " + ex.getMessage());
        }

        this.jobsProcessed++;

    }
}
