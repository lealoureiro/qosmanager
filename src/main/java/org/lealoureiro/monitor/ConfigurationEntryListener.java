package org.lealoureiro.monitor;

import org.apache.log4j.Logger;
import org.snmp4j.agent.mo.MOTableRowEvent;
import org.snmp4j.agent.mo.MOTableRowListener;

/**
 * Configuration MIB Table Actions Listener
 *
 * @author Leandro Loureiro <lealoureiro@gmail.com>
 * @version 1.0.0
 */

public class ConfigurationEntryListener implements MOTableRowListener {

    private static Logger log = Logger.getLogger(ConfigurationEntryListener.class);

    private CoreApp app;
    private ConfigurationTableModel configModel;

    public ConfigurationEntryListener(CoreApp app, ConfigurationTableModel configModel) {
        this.app = app;
        this.configModel = configModel;
    }

    public void rowChanged(MOTableRowEvent motre) {
        if (motre.getType() == MOTableRowEvent.UPDATED) {
            if (motre.getRow().getIndex().toString().equals("1")) { // add new probe

                log.info("Adding new probe " + motre.getRow().getValue(0).toString() + " ...");
                try {

                    this.app.addNewProbe(motre.getRow().getValue(0).toString(), motre.getRow().getValue(1).toString(), Integer.parseInt(motre.getRow().getValue(2).toString()), motre.getRow().getValue(3).toString(), motre.getRow().getValue(4).toString(), motre.getRow().getValue(5).toString());

                } catch (Exception ex) {
                    this.configModel.setErrorMessage(1, ex.getMessage());
                    log.warn("Failed to add new probe: " + ex.getMessage());
                }

            } else if (motre.getRow().getIndex().toString().equals("2")) { // delete probe
                log.info("Delete probe " + motre.getRow().getValue(0).toString() + " ...");

                try {
                    this.app.deleteProbe(motre.getRow().getValue(0).toString());
                } catch (Exception ex) {
                    this.configModel.setErrorMessage(2, ex.getMessage());
                    log.warn("Failed to delete new probe: " + ex.getMessage());
                }
            } else if (motre.getRow().getIndex().toString().equals("3")) {
                log.info("Change probe " + motre.getRow().getValue(0).toString() + " polling time...");

                try {
                    this.app.changeProbePollingTime(motre.getRow().getValue(0).toString(), Integer.parseInt(motre.getRow().getValue(1).toString()));
                } catch (Exception ex) {
                    this.configModel.setErrorMessage(3, ex.getMessage());
                    log.warn("Failed to set probe polling time: " + ex.getMessage());
                }

            } else if (motre.getRow().getIndex().toString().equals("4")) {
                log.info("Change probe " + motre.getRow().getValue(0).toString() + " access credentials...");

                try {
                    this.app.changeProbeCredentials(motre.getRow().getValue(0).toString(), motre.getRow().getValue(1).toString(), motre.getRow().getValue(2).toString(), motre.getRow().getValue(3).toString());
                } catch (Exception ex) {
                    this.configModel.setErrorMessage(4, ex.getMessage());
                    log.warn("Failed to set probe credentials: " + ex.getMessage());
                }

            } else if (motre.getRow().getIndex().toString().equals("5")) {
                log.info("Adding new job " + motre.getRow().getValue(0).toString() + " ...");

                try {
                    this.app.addJob(motre.getRow().getValue(0).toString(), Integer.parseInt(motre.getRow().getValue(1).toString()), Integer.parseInt(motre.getRow().getValue(2).toString()));
                } catch (Exception ex) {
                    this.configModel.setErrorMessage(5, ex.getMessage());
                    log.warn("Failed to add job : " + ex.getMessage());
                }
            }
        }
    }
}
