package org.lealoureiro.monitor;

import org.snmp4j.agent.mo.MOMutableTableRow;
import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.Variable;

/**
 * Configuration MIB Entry
 *
 * @author Leandro Loureiro <lealoureiro@gmail.com>
 * @version 1.0.0
 */

public class ConfigurationMIBEntry implements MOMutableTableRow, Comparable {

    private OID index;
    private OctetString values[];
    public static int maxConfigFields = 8;

    public ConfigurationMIBEntry(int index, String[] configNames) {

        this.index = new OID("" + index);
        this.values = new OctetString[ConfigurationMIBEntry.maxConfigFields];

        int i;
        for (i = 0; i < configNames.length; i++) {
            values[i] = new OctetString(configNames[i]);
        }

        for (; i < (ConfigurationMIBEntry.maxConfigFields - 1); i++) {
            this.values[i] = new OctetString("not_in_use");
        }

        this.values[ConfigurationMIBEntry.maxConfigFields - 1] = new OctetString("no_error");

    }

    public void setValue(int i, Variable vrbl) {
        this.values[i] = (OctetString) vrbl;
    }

    public void setBaseRow(MOTableRow motr) {
    }

    public OID getIndex() {
        return this.index;
    }

    public Variable getValue(int i) {
        return this.values[i];
    }

    public MOTableRow getBaseRow() {
        return null;
    }

    public int size() {
        return ConfigurationMIBEntry.maxConfigFields;
    }

    public int compareTo(Object t) {
        return getIndex().compareTo(((MOMutableTableRow) t).getIndex());
    }
}
