package org.lealoureiro.monitor;

import org.snmp4j.agent.mo.*;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.SMIConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * MIB Table for List Current Working Probes
 *
 * @author Leandro Loureiro <lealoureiro@gmail.com>
 * @version 1.0.0
 */
public class ProbesTable {

    private List<MOColumn> columns;
    private MOTableSubIndex[] subIndexes;
    private MOTableIndex indexDef;
    private DefaultMOTable table;
    private ProbesTableModel model;

    public ProbesTable() {
        this.columns = new ArrayList<MOColumn>();
        this.subIndexes = new MOTableSubIndex[]{new MOTableSubIndex(SMIConstants.SYNTAX_INTEGER)};
    }

    public ProbesTableModel getTableModel() {
        return this.model;
    }

    public void initTable() {

        this.columns.add(new MOColumn(1, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_ONLY)); // probe id/name
        this.columns.add(new MOColumn(2, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_ONLY)); // probe address
        this.columns.add(new MOColumn(3, SMIConstants.SYNTAX_GAUGE32, MOAccessImpl.ACCESS_READ_ONLY));      // probe port
        this.columns.add(new MOColumn(4, SMIConstants.SYNTAX_GAUGE32, MOAccessImpl.ACCESS_READ_ONLY));      // probe polling time
        this.columns.add(new MOColumn(5, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_ONLY)); // probe username
        this.columns.add(new MOColumn(6, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_ONLY)); // probe password
        this.columns.add(new MOColumn(7, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_ONLY)); // probe passphrase
        this.columns.add(new MOColumn(8, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_ONLY)); // probe last error

        this.indexDef = new MOTableIndex(subIndexes, false);

        this.table = new DefaultMOTable(new OID(".1.3.6.1.3.1"), this.indexDef, this.columns.toArray(new MOColumn[0]));
        this.table.setVolatile(false);

        this.model = new ProbesTableModel();
        this.table.setModel(model);

    }

    public DefaultMOTable getTableObject() {
        return this.table;
    }
}
