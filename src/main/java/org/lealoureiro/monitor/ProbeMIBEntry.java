
package org.lealoureiro.monitor;

import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.smi.Gauge32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.Variable;

/**
 * MIB Probe Table Entry Class
 *
 * @author Leandro Loureiro <lealoureiro@gmail.com>
 * @version 1.0.0
 */

public class ProbeMIBEntry implements MOTableRow, Comparable {

    private OID index;


    private Variable[] values;

    public ProbeMIBEntry(OID index, String probeId, String address, int port, int pollingTime, String authUsername, String authPassword, String authPassphrase) {

        this.index = index;
        this.values = new Variable[8];
        this.values[0] = new OctetString(probeId);
        this.values[1] = new OctetString(address);
        this.values[2] = new Gauge32(port);
        this.values[3] = new Gauge32(pollingTime);
        this.values[4] = new OctetString(authUsername);
        this.values[5] = new OctetString(authPassword);
        this.values[6] = new OctetString(authPassphrase);
        this.values[7] = new OctetString("no_error");

    }

    public void setPollingTime(int pollingTime) {
        this.values[2] = new Gauge32(pollingTime);
    }

    public void setAuthUsername(String authUsername) {
        this.values[4] = new OctetString(authUsername);
    }

    public void setAuthPassword(String authPassword) {
        this.values[5] = new OctetString(authPassword);
    }

    public void setAuthPassphrase(String authPassphrase) {
        this.values[6] = new OctetString(authPassphrase);
    }

    public void setErrorMessage(String message) {
        this.values[7] = new OctetString(message);
    }

    public OID getIndex() {
        return this.index;
    }

    public Variable getValue(int i) {
        return this.values[i];
    }

    public MOTableRow getBaseRow() {
        return null;
    }

    public void setBaseRow(MOTableRow motr) {

    }

    public int size() {
        return this.values.length;
    }

    public int compareTo(Object t) {
        return getIndex().compareTo(((MOTableRow) t).getIndex());
    }

}
