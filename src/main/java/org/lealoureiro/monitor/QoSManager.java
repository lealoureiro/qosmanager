package org.lealoureiro.monitor;

import com.mysql.jdbc.log.Slf4JLogger;
import org.apache.log4j.Logger;
import org.lealoureiro.monitor.db.DatabaseAccess;

import java.io.IOException;
import java.sql.SQLException;

/**
 * QoS SNMP v3 Manager
 *
 * @author Leandro Loureiro <leaoureiro@gmail.com>
 * @version 1.0.0
 */

public class QoSManager {

    private static Logger log = Logger.getLogger(QoSManager.class);

    public static void main(String[] args) {

        log.info("QoS Manager v1.0.0");
        try {
            log.info("Loading MySQL Driver...");
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            log.fatal("MySQL init Driver Problem: " + ex);
            System.exit(1);
        }

        log.info("Connecting to database...");
        DatabaseAccess dbAccess = new DatabaseAccess("172.16.190.132", "mainuser", "asd123", "qos");
        try {
            dbAccess.init();
        } catch (SQLException ex) {
            log.fatal("Failed to connect database: " + ex.getMessage());
            System.exit(1);
        }

        log.info("Init MIB Probe Table");
        ProbesTable probeTable = new ProbesTable();
        probeTable.initTable();


        log.info("Init Application Core");
        CoreApp app = new CoreApp(dbAccess, probeTable);

        log.info("Init MIB Manager Configuration Table");
        ConfigurationTable configTable = new ConfigurationTable(app);
        configTable.initTable();

        log.info("Init SNMP v3 Agent on UDP port 2001");
        ManagerAgent agent = new ManagerAgent("udp:0.0.0.0/2001", 5);
        agent.registerManagedObject(probeTable.getTableObject());
        agent.registerManagedObject(configTable.getTableObject());


        try {
            log.info("Starting Agent");
            agent.start();
        } catch (IOException ex) {
            log.fatal("Failed to start Manager Agent: " + ex.getMessage());
        }

        log.info("Loading probes list from storage");
        app.initProbesConnectors();

    }
}
