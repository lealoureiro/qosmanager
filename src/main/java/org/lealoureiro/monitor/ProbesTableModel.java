package org.lealoureiro.monitor;

import org.snmp4j.agent.mo.MOTableModel;
import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.smi.OID;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * MIB Probes Table Model
 *
 * @author Leandro Loureiro <lealoureiro@gmail.com>
 * @version 1.0.0
 */

public class ProbesTableModel implements MOTableModel {

    private TreeMap<OID, ProbeMIBEntry> probesList;

    public ProbesTableModel() {
        this.probesList = new TreeMap<OID, ProbeMIBEntry>();
    }

    public int getColumnCount() {
        return 8;
    }

    public int getRowCount() {
        return this.probesList.size();
    }

    public boolean containsRow(OID oid) {
        return this.probesList.containsKey(oid);
    }

    public MOTableRow getRow(OID oid) {
        return this.probesList.get(oid);
    }

    public Iterator iterator() {
        return this.probesList.values().iterator();
    }

    public Iterator tailIterator(OID oid) {
        if (oid == null) {
            return iterator();
        }
        return this.probesList.tailMap(oid).values().iterator();
    }

    public OID lastIndex() {
        if (this.probesList.size() > 0) {
            return probesList.lastKey();
        }
        return null;
    }

    public OID firstIndex() {
        if (this.probesList.size() > 0) {
            return this.probesList.firstKey();
        }
        return null;
    }

    public MOTableRow firstRow() {
        OID index = firstIndex();
        if (index != null) {
            return this.probesList.get(index);
        }
        return null;
    }

    public MOTableRow lastRow() {
        OID index = lastIndex();
        if (index != null) {
            return this.probesList.get(index);
        }
        return null;
    }

    public synchronized void removeRow(String probeId) {
        OID index = this.getOIDByProbeId(probeId);
        if (index != null) {
            this.probesList.remove(index);
        }
    }

    private OID getOIDByProbeId(String probeId) {
        OID index = null;
        for (Map.Entry<OID, ProbeMIBEntry> e : this.probesList.entrySet()) {
            if (e.getValue().getValue(0).toString().equals(probeId)) {
                index = e.getKey();
            }
        }
        return index;
    }

    public void addRow(String probeId, String address, int port, int pollingTime, String authUsername, String authPassword, String authPassphrase) {
        int i = 0;

        while (this.containsRow(new OID("" + i))) {
            i++;
        }

        ProbeMIBEntry entry = new ProbeMIBEntry(new OID("" + i), probeId, address, port, pollingTime, authUsername, authPassword, authPassphrase);
        this.probesList.put(new OID("" + 1), entry);
    }

    public void setPollingTime(String probeId, int pollingTime) {
        OID index = this.getOIDByProbeId(probeId);
        if (index != null) {
            this.probesList.get(index).setPollingTime(pollingTime);
        }
    }

    public void setAuthUsername(String probeId, String authUsername) {
        OID index = this.getOIDByProbeId(probeId);
        if (index != null) {
            this.probesList.get(index).setAuthUsername(authUsername);
        }
    }

    public void setAuthPassword(String probeId, String authPassword) {
        OID index = this.getOIDByProbeId(probeId);
        if (index != null) {
            this.probesList.get(index).setAuthPassword(authPassword);
        }
    }

    public void setAuthPassphrase(String probeId, String authPassphrase) {
        OID index = this.getOIDByProbeId(probeId);
        if (index != null) {
            this.probesList.get(index).setAuthPassphrase(authPassphrase);
        }
    }

    public void setErrorMessage(String probeId, String message) {
        OID index = this.getOIDByProbeId(probeId);
        if (index != null) {
            this.probesList.get(index).setErrorMessage(message);
        }
    }
}
