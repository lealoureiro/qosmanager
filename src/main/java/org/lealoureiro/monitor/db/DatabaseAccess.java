package org.lealoureiro.monitor.db;

import org.lealoureiro.monitor.QoSProbeConnector;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Class for Database Manipulation
 *
 * @author Leandro Loureiro <lealoureiro@gmail.com>
 * @verison 1.0.0
 */
public class DatabaseAccess {

    private String hostname;
    private String username;
    private String password;
    private String database;
    private Connection connnection;

    public DatabaseAccess(String hostname, String username, String password, String database) {
        this.hostname = hostname;
        this.username = username;
        this.password = password;
        this.database = database;
    }

    public void init()
            throws SQLException {

        StringBuilder str = new StringBuilder();
        str.append("jdbc:mysql://");
        str.append(this.hostname);
        str.append("/");
        str.append(this.database);
        str.append("?user=");
        str.append(this.username);
        str.append("&password=");
        str.append(this.password);

        this.connnection = java.sql.DriverManager.getConnection(str.toString());
    }

    public int createReportEntry(String probeId, String ipAddress)
            throws SQLException {

        int id = 0;

        StringBuilder str = new StringBuilder();

        str.append("INSERT INTO results(probe, ip) VALUES('");
        str.append(probeId);
        str.append("','");
        str.append(ipAddress);
        str.append("');");

        Statement smt = this.connnection.createStatement();
        smt.executeUpdate(str.toString(), Statement.RETURN_GENERATED_KEYS);
        ResultSet rs = smt.getGeneratedKeys();
        if (rs.next()) {
            id = rs.getInt(1);
        }
        rs.close();
        smt.close();

        return id;
    }

    public synchronized void storeProbeData(String probeId, String ip, int port, String username, String password, String passphrase, int pollingTime)
            throws SQLException {


        StringBuilder str = new StringBuilder();
        str.append("INSERT INTO probes(probeId,ip,port,username,password,passphrase,pollingTime) VALUES('");
        str.append(probeId);
        str.append("','");
        str.append(ip);
        str.append("',");
        str.append(port);
        str.append(",'");
        str.append(username);
        str.append("','");
        str.append(password);
        str.append("','");
        str.append(passphrase);
        str.append("',");
        str.append(pollingTime);
        str.append(")");

        Statement smt = this.connnection.createStatement();
        smt.executeUpdate(str.toString(), Statement.RETURN_GENERATED_KEYS);

    }

    public synchronized ArrayList<QoSProbeConnector> getStoredProbes()
            throws SQLException {

        ArrayList<QoSProbeConnector> probes = new ArrayList<QoSProbeConnector>();

        Statement smt = this.connnection.createStatement();
        ResultSet rs = smt.executeQuery("SELECT probeId,ip,port,username,password,passphrase FROM probes;");

        while (rs.next()) {
            QoSProbeConnector probe = new QoSProbeConnector(rs.getString(1), rs.getString(2), rs.getInt(3), this);
            probe.setCredentials(rs.getString(4), rs.getString(5), rs.getString(6));
            probes.add(probe);
        }

        return probes;
    }

    public synchronized void storeReportData(String jobId, String timestamp, String rttNow, String rttMax, String rttMin, String rttAvg, String packetLoss, String jitter, String alive)
            throws SQLException {

        StringBuilder str = new StringBuilder();
        str.append("UPDATE results SET  timestamp=");
        str.append(timestamp);
        str.append(",rttnow=");
        str.append(rttNow);
        str.append(",rttmax=");
        str.append(rttMax);
        str.append(",rttmin=");
        str.append(rttMin);
        str.append(",rttavg=");
        str.append(rttAvg);
        str.append(",packetloss=");
        str.append(packetLoss);
        str.append(",jitter=");
        str.append(jitter);
        str.append(",complete=1,alive=");
        str.append(alive);
        str.append(" WHERE id=");
        str.append(jobId);

        Statement smt = this.connnection.createStatement();
        smt.executeUpdate(str.toString());

    }


    public synchronized void setProbeCredentials(String probeId, String username, String password, String passphrase)
            throws SQLException {

        StringBuilder str = new StringBuilder();

        str.append("UPDATE probes SET username='");
        str.append(username);
        str.append("',password='");
        str.append(password);
        str.append("',passphrase='");
        str.append(passphrase);
        str.append("' WHERE probeId='");
        str.append(probeId);
        str.append("'");

        Statement smt = this.connnection.createStatement();
        smt.executeUpdate(str.toString());
    }

    public synchronized void setProbePollingTime(String probeId, int pollingTime)
            throws SQLException {

        StringBuilder str = new StringBuilder();

        str.append("UPDATE probes SET pollingTime=");
        str.append(pollingTime);
        str.append(" WHERE probeId='");
        str.append(probeId);
        str.append("'");

        Statement smt = this.connnection.createStatement();
        smt.executeUpdate(str.toString());
    }


    public synchronized void removeProbe(String probeId)
            throws SQLException {

        StringBuilder str = new StringBuilder();
        str.append("DELETE FROM probes WHERE probeId='");
        str.append(probeId);
        str.append("'");

        Statement smt = this.connnection.createStatement();
        smt.executeUpdate(str.toString());
    }

}
