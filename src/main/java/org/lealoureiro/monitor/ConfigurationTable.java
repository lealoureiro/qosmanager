package org.lealoureiro.monitor;

import org.snmp4j.agent.mo.*;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.SMIConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * QoS Manager MIB Configuration Table
 *
 * @author Leandro Loureiro <lealoureiro@gmail.com>
 * @version 1.0.0
 */

public class ConfigurationTable {

    private CoreApp app;
    private List<MOColumn> columns;
    private MOTableIndex indexDef;
    private MOTableSubIndex[] subIndexes;
    private DefaultMOTable table;
    private ConfigurationTableModel model;
    private ConfigurationEntryListener reporter;

    public ConfigurationTable(CoreApp app) {
        this.app = app;
        this.columns = new ArrayList<MOColumn>();
        this.subIndexes = new MOTableSubIndex[]{new MOTableSubIndex(SMIConstants.SYNTAX_INTEGER)};
        this.indexDef = new MOTableIndex(subIndexes, false);
        this.model = new ConfigurationTableModel();
        this.reporter = new ConfigurationEntryListener(this.app, this.model);
    }

    public void initTable() {
        this.columns.add(new MOMutableColumn(1, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_WRITE));
        this.columns.add(new MOMutableColumn(2, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_WRITE));
        this.columns.add(new MOMutableColumn(3, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_WRITE));
        this.columns.add(new MOMutableColumn(4, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_WRITE));
        this.columns.add(new MOMutableColumn(5, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_WRITE));
        this.columns.add(new MOMutableColumn(6, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_WRITE));
        this.columns.add(new MOMutableColumn(7, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_WRITE));
        this.columns.add(new MOMutableColumn(8, SMIConstants.SYNTAX_OCTET_STRING, MOAccessImpl.ACCESS_READ_WRITE)); // last error

        this.table = new DefaultMOTable(new OID(".1.3.6.1.3.2"), this.indexDef, this.columns.toArray(new MOColumn[0]));
        this.table.setModel(this.model);
        this.table.addMOTableRowListener(this.reporter);
    }

    public DefaultMOTable getTableObject() {
        return this.table;
    }
}
