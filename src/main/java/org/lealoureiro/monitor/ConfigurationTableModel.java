
package org.lealoureiro.monitor;

import org.snmp4j.agent.mo.MOTableModel;
import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;

import java.util.Iterator;
import java.util.TreeMap;

/**
 * QoS Manager Configuration Table Model
 *
 * @author Leandro Loureiro <lealoureiro@gmail.com>
 * @version 1.0.0
 */

public class ConfigurationTableModel implements MOTableModel {

    private TreeMap<OID, ConfigurationMIBEntry> configs;

    public ConfigurationTableModel() {
        this.configs = new TreeMap<OID, ConfigurationMIBEntry>();

        // entry for setting new probe
        String[] configs1 = {"probe_id", "ip", "port", "polling", "username", "password", "passprhase"};
        ConfigurationMIBEntry c1 = new ConfigurationMIBEntry(1, configs1);
        this.configs.put(c1.getIndex(), c1);

        // entry for delete probe
        String[] configs2 = {"probe_id"};
        ConfigurationMIBEntry c2 = new ConfigurationMIBEntry(2, configs2);
        this.configs.put(c2.getIndex(), c2);

        // entry for change polling time
        String[] configs3 = {"probe_id", "new_polling_time"};
        ConfigurationMIBEntry c3 = new ConfigurationMIBEntry(3, configs3);
        this.configs.put(c3.getIndex(), c3);

        // entry for setting new access credentials
        String[] configs4 = {"probe_id", "new_username", "new_password", "new_passphrase"};
        ConfigurationMIBEntry c4 = new ConfigurationMIBEntry(4, configs4);
        this.configs.put(c4.getIndex(), c4);

        // entry for add new job for simulate streaming server requests
        String[] configs5 = {"host_ip", "requests", "polling"};
        ConfigurationMIBEntry c5 = new ConfigurationMIBEntry(5, configs5);
        this.configs.put(c5.getIndex(), c5);

    }

    public int getColumnCount() {
        return ConfigurationMIBEntry.maxConfigFields;
    }

    public int getRowCount() {
        return this.configs.size();
    }

    public boolean containsRow(OID oid) {
        return this.configs.containsKey(oid);
    }

    public MOTableRow getRow(OID oid) {
        return this.configs.get(oid);
    }

    public Iterator iterator() {
        return this.configs.values().iterator();
    }

    public Iterator tailIterator(OID oid) {
        if (oid == null) {
            return iterator();
        }
        return this.configs.tailMap(oid).values().iterator();
    }

    public OID lastIndex() {
        if (this.configs.size() > 0) {
            return configs.lastKey();
        }
        return null;
    }

    public OID firstIndex() {
        if (this.configs.size() > 0) {
            return this.configs.firstKey();
        }
        return null;
    }

    public MOTableRow firstRow() {
        OID index = firstIndex();
        if (index != null) {
            return this.configs.get(index);
        }
        return null;
    }

    public MOTableRow lastRow() {
        OID index = lastIndex();
        if (index != null) {
            return this.configs.get(index);
        }
        return null;
    }

    public void setErrorMessage(int rowIndex, String message) {
        ConfigurationMIBEntry configEntry = this.configs.get(new OID("" + rowIndex));
        if (configEntry != null) {
            configEntry.setValue(7, new OctetString(message));
        }
    }


}
